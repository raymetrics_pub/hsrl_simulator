.. Lidar Simulation Library documentation master file, created by
   sphinx-quickstart on Wed Dec 12 09:52:39 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Lidar Simulation Library's documentation!
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   simulation
   optics
   atmosphere
   electronics
   geometry



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
