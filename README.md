# Introduction

A script to simulate signals of a High Spectral Resolution Lidar (HSRL)

# Installation

You can install the module using the `pip` installation tool e.g.:

```
pip install <local_code_path>
```

# Acknowledgments
Co‐financed by the European Union and Greek national funds
through the Operational Program Competitiveness, Entrepreneurship and Innovation,
under the call RESEARCH – CREATE - INNOVATE (project code:Τ1EDK-03398)

![EU logo](logos/espalogos-eng.png)
