"""
   Utility functions for optical geometry calculations.
"""

import pandas as pd
from scipy.interpolate import interp1d


def import_overlap(overlap_filename, distances, interp_kind='linear'):
    """

    Parameters
    ----------
    overlap_filename: str
        Path to an atmosphere description file
    distances: (M,) array
        Distance vector in meters
    interp_kind: str
        Interpolation type supported by scipy interp1d

    Returns
    -------
    overlap: (M,) array
        Overlap function

    """
    df = pd.read_csv(overlap_filename)
    file_distances = df['Range(meters)'].values
    file_overlap = df['Overlap'].values
    overlap_interpolator = interp1d(file_distances, file_overlap, fill_value='extrapolate', kind=interp_kind)
    overlap = overlap_interpolator(distances)
    return overlap
