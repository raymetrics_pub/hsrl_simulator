"""
   Utility functions for the calculation of atmospheric parameters.
"""

import numpy as np
import pandas as pd
from lidar_processing import helper_functions
from scipy import constants
from scipy.interpolate import interp1d

from lidarsimulator.utils.geometry import import_overlap
from lidarsimulator.utils.optics import calculate_transmission


class Atmosphere(object):
    """
    Atmosphere class.
    Fields:
        distances : (M,) array
            Absorbtion coefficient for each bin
        altitudes : (M,) array
            Absorbtion coefficient for each bin
        overlap: (M,) array
            Overlap function
        wavelength: float
        a_mol : (M,) array
            Absorbtion coefficient for each bin
        b_mol : (M,) array
            Backscatter matrix for each bin
        a_aer : (M,) array
            Absorbtion coefficient for each bin
        b_aer : (M,) array
            Backscatter matrix for each bin
    """

    def __init__(self, aerosol_file, overlap_file, settings):
        self.distances, self.altitudes = calculate_bins(settings['Recorder']['SamplingRate'] * 1e6,
                                                        settings['Recorder']['RangeBins'],
                                                        settings['Atmosphere']['SystemZenithAngle'],
                                                        settings['Atmosphere']['SystemGroundLevel'])
        self.overlap = import_overlap(overlap_file, self.distances)
        self.wavelength = settings['Laser']['LaserWavelength']

        self.b_mol, self.a_mol = calculate_standard_atmosphere(self.altitudes, settings['Atmosphere'], self.wavelength)

        self.b_aer, self.a_aer = \
            import_atmosphere(aerosol_file, self.altitudes, settings['Atmosphere'], self.wavelength)
        self.constituents = None

        self.atm_t = calculate_transmission(self.a_mol + self.a_aer, self.distances[1] - self.distances[0])


def calculate_bins(sampling_rate, bin_count, zenith, ground_level=0):
    """
    Calculates the altitude and distance from the system for each bin.

    Parameters
    ----------
    sampling_rate: float
        System sampling rate in Hz
    bin_count: int
        Total number of recorded bins
    zenith: float
        Zenith angle of the system
    ground_level: float
        Ground level offset for altitude calculation, ignore if zero.

    Returns
    -------
    distances: (M,) array
        Distance from the system for each bin
    altitudes: (M,) array
        Altitude for each bin (assuming the system is at sea level)

    """
    tbin = 1.0 / sampling_rate
    time = np.linspace(tbin, tbin * bin_count, bin_count, endpoint=False)
    distances = (constants.c * time) / 2
    altitudes = distances * np.cos(np.deg2rad(zenith)) + ground_level
    return distances, altitudes


def import_atmosphere(atmosphere_filename, altitudes, atm_settings, wavelength, raman_wavelength=None):
    """
    Imports an atmosphere configuration from a csv file.

    Parameters
    ----------
    atmosphere_filename: str
        Filename for the atmosphere description csv file.
    altitudes: (M,) array
        Altitude values for each bin.
    atm_settings: dict
        The atmosphere section of a simulation settings dictionary
    wavelength: float
        Emission wavelength in nm
    raman_wavelength: float
        Raman wavelength in nm, ignore if not used.

    Returns
    -------
    b_aer: (M,) array
        Aerosol backscatter coefficient for each bin
    a_aer_355: (M,) array
        Aerosol extinction coefficient at 355nm for each bin
    a_aer_387: (M,) array
        Aerosol extinction coefficient at 387nm for each bin
    ld_aer: (M,) array
        Aerosol linear depolarization for each bin

    """
    df = pd.read_csv(atmosphere_filename)
    file_a_aer = df['α@emission'].values
    file_b_aer = np.divide(file_a_aer, df['LR'].values)
    file_altitudes = df['Altitude(meters)']

    a_aer_interpolator = interp1d(file_altitudes, file_a_aer, fill_value='extrapolate')
    b_aer_interpolator = interp1d(file_altitudes, file_b_aer, fill_value='extrapolate')

    a_aer = a_aer_interpolator(altitudes)

    b_aer = b_aer_interpolator(altitudes)

    return b_aer, a_aer


def calculate_standard_atmosphere(altitudes, atm_settings, wavelength, raman_wavelength=None):
    """
    Calculates molecular properties using the standard atmosphere model.

    Parameters
    ----------
    altitudes: (M,) array
        Altitude values for each bin.
    atm_settings: dict
        The atmosphere section of a simulation settings dictionary
    wavelength: float
        Emission wavelength in nm
    raman_wavelength: float
        Raman wavelength in nm, ignore if not used.

    Returns
    -------
    b_mol: (M,) array
        Molecular backscatter coefficient for each bin
    a_mol: (M,) array
        Molecular extinction coefficient at 355 nm for each bin
    a_mol_raman: (M,) array
        Molecular extinction coefficient at 387 nm for each bin
    d_mol: (M,) array
        Molecular linear depolarization coefficient for each bin

    """

    pressure, temperature, _ = helper_functions.standard_atmosphere(altitudes)
    b_mol = helper_functions.molecular_backscatter(wavelength, pressure, temperature, component='cabannes')
    a_mol = helper_functions.molecular_extinction(wavelength, pressure, temperature)

    return b_mol, a_mol
