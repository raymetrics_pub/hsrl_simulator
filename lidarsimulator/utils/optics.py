"""
   Utility functions for the calculation of optical parameters and signals.
"""

import numpy as np
from scipy import constants, integrate


def calculate_solar_bg_photons(fov_full, area, solar_bg, wavelength, iff_bw):
    """
    Calculates the solar background signal.

    Parameters
    ----------
    fov_full: float
        Telescope field of view (full-angle) in radians
    area: float
        Telescope area in m^2
    solar_bg:
        Solar radiation intensity in W/(nm*sr*m^2)
    wavelength:
        Wavelength in nm
    iff_bw:
        Interference filter bandwidth in nm

    Returns
    -------
    cps: float
        Expected background signal in counts per second

    """
    power = solar_bg * area * iff_bw * 2 * constants.pi * (fov_full / 2) ** 2
    cps = power * (wavelength * 1e-9) / (constants.h * constants.c)
    return cps


def calculate_transmission(alpha, bin_length):
    """
    Transmission calculation based on Weitcamp Eq. 4.1

    Parameters
    ----------
    alpha: : (M,) array
        Extinction coefficient for each bin
    bin_length: float
        Bin length in meters

    Returns
    -------
    T:
        Transmission for the full or half trip

    """
    """"""
    a_int = integrate.cumtrapz(alpha, dx=bin_length, initial=0)
    return np.exp(-2 * a_int)


def calculate_return_signal(channel_name, atmosphere, settings):
    """
    Calculates the expected return signal for the specified channel.

    Parameters
    ----------
    channel_name: str
        Name of the channel to be calculated
    atmosphere: Atmosphere
        Atmosphere object calculated for the system
    settings: dict
        The simulation settings dictionary


    Returns
    -------
    cps: (M,) array
        Signal for the specified channel in counts per second
    """

    laser_settings = settings['Laser']
    optics_settings = settings['Optics']
    atmosphere_settings = settings['Atmosphere']

    telescope_area = np.pi * ((optics_settings['TelescopeAperture'] / 2) ** 2 -
                              (optics_settings['TelescopeObscuration'] / 2) ** 2)

    n_phot = (laser_settings['LaserEnergy'] * 1e-3) * (
            laser_settings['LaserWavelength'] * 1e-9 / (constants.h * constants.c))
    system = (constants.c / 2) * telescope_area * atmosphere.overlap * atmosphere.distances ** -2

    channel_type = optics_settings['Channels'][channel_name]['Type']

    if channel_type == 'Total':
        tot_beta = atmosphere.b_mol + atmosphere.b_aer
        lp_bg_cps = calculate_solar_bg_photons(optics_settings['FOV_FullAngle'], telescope_area,
                                               atmosphere_settings['SolarBG']['Center'], atmosphere.wavelength,
                                               optics_settings['Channels'][channel_name]['IFF_BW'])
        lp_cps = (n_phot * system * tot_beta * atmosphere.atm_t + lp_bg_cps
                  ) * optics_settings['Channels'][channel_name]['PathTransmission']

        cps = lp_cps

    elif channel_type == 'HSRL':
        c_am = optics_settings['Channels'][channel_name]['Ca']
        c_mm = optics_settings['Channels'][channel_name]['Cm']
        hsrl_beta = c_am * atmosphere.b_aer + c_mm * atmosphere.b_mol
        hsrl_bg_cps = calculate_solar_bg_photons(optics_settings['FOV_FullAngle'], telescope_area,
                                                 atmosphere_settings['SolarBG']['Center'], atmosphere.wavelength,
                                                 optics_settings['Channels'][channel_name]['IFF_BW'])
        hsrl_cps = (n_phot * system * hsrl_beta * atmosphere.atm_t +
                    hsrl_bg_cps) * optics_settings['Channels'][channel_name]['PathTransmission']

        cps = hsrl_cps

    else:
        raise NotImplementedError("Unknown channel type!")

    return cps
