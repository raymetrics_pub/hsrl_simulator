"""
   Utility functions for PMT sensor simulation.
"""

import numpy as np
from scipy import constants


def calculate_an_pc_constant(detector_settings, recorder_settings, mhz=False):
    """
    Calculates the analog to photon counting signal conversion ratio for a given sensor.
    Parameters
    ----------
    detector_settings: dict
        The detector section of a simulation settings dictionary
    recorder_settings: dict
        The recorder section of a simulation settings dictionary
    mhz: bool
        If True the ratio is calculated for signals in MHz and otherwise in counts per bin.

    Returns
    -------
    an_pc_ratio: float
        Analog to photon counting conversion ratio

    """
    pc_coeff = detector_settings['QuantumEfficiency'] / (recorder_settings['SamplingRate'] * 1e6)
    an_coeff = (constants.h * constants.c / 355e-9) * (detector_settings['CathodeResponsivity'] *
                                                       detector_settings['Gain'] * detector_settings['Load'] * 1000)
    if mhz:
        pc_coeff = pc_coeff * recorder_settings['SamplingRate']
    an_pc_ratio = an_coeff / pc_coeff
    return an_pc_ratio


def calculate_signal(cps, settings, sig_noise=False):
    """
    Calculates an analog and photon counting signal based on the expected light on the sensor.
    Parameters
    ----------
    cps: (M,) array
        Expected light per bin in photons per second
    settings: dict
        The simulation settings dictionary
    sig_noise: bool
        Enables or disables signal noise. Default: False

    Returns
    -------
    pc_mhz: (M,) array
        Photon counting signal in MHz
    pc_noise_mhz: (M,) array
        Photon counting signal  standard deviation in MHz
    an_mvolts: (M,) array
        Analog signal in mV
    n_noise_mvolts: (M,) array
        Analog signal standard deviation in mV

    """
    laser_settings = settings['Laser']
    detector_settings = settings['Detector']
    recorder_settings = settings['Recorder']
    nshots = recorder_settings['AveragingTime'] * laser_settings['RepetitionRate']
    dt = float(detector_settings['DeadTime']) * 1e-9

    pc_single_shot = (cps + detector_settings['DarkCps']) * detector_settings['QuantumEfficiency'] / \
                     (recorder_settings['SamplingRate'] * 1e6)
    if sig_noise:
        pc_total = pc_single_shot * nshots
        pc_single_shot = np.random.poisson(pc_total) / nshots
    pc_noise = np.sqrt(pc_single_shot) / np.sqrt(nshots)
    pc_hz = pc_single_shot * recorder_settings['SamplingRate'] * 1e6
    pc_hz = pc_hz / (1 + pc_hz * dt)

    pc_mhz = pc_hz / 1e6
    pc_noise_mhz = pc_noise * recorder_settings['SamplingRate']

    an_mvolts = (cps * (constants.h * constants.c / (laser_settings['LaserWavelength'] * 1e-9))
                 * detector_settings['CathodeResponsivity'] * detector_settings['Gain'] +
                 detector_settings['DarkCurrent'] * 1e-9) * detector_settings['Load'] * 1000

    an_noise_mvolts = pc_noise * calculate_an_pc_constant(detector_settings, recorder_settings)
    return pc_mhz, pc_noise_mhz, an_mvolts, an_noise_mvolts
