import matplotlib.pyplot as plt
import pandas as pd
import yaml

from lidarsimulator.utils import electronics
from lidarsimulator.utils import optics
from lidarsimulator.utils.atmosphere import Atmosphere

"""
A module containing classes for running simulations, plotting and saving the results.
"""


class LidarSimulator(object):
    """
    A class to handle the execution of a simulation and data exporting.

    Attributes
    ----------
    settings : dict
        The settings dictionary the simulator was initialized with.
    """

    def __init__(self, aerosol_file, overlap_file, settings_file):
        """
        Constructor for a simulator object

        Parameters
        ----------
        settings_file : str
            A path to a YAML configuration file containing the lidar setup.

        Raises
        ------
        IOError
            Raised if the path to the lidar configuration file is invalid
        AssertionError
            Raised if the lidar configuration file fails validation.
        """
        with open(settings_file) as stream:
            self.settings = yaml.safe_load(stream)
            self.atmosphere = Atmosphere(aerosol_file, overlap_file, self.settings)

    def runall_light(self, plot='none', output_file=None):
        """
        Runs a simulation of the expected light returning to the sensor.

        Parameters
        ----------
        plot : str
            Type of plot to draw, use 'signals' or 'none' (default).
        output_file : str
            Path for the output csv file, ignore to run without saving.
        Returns
        -------
        channels : list of str
            List of channel names.
        signals : list of ndarray
            List of signals (photons/sec) as ndarrays.
        """

        signals = []
        channels = list(self.settings['Optics']['Channels'].keys())
        if not plot == 'none':
            plt.figure()

        for channel in channels:
            cps = optics.calculate_return_signal(channel, self.atmosphere, self.settings)
            signals.append(cps)
            if not plot == 'none':
                plt.semilogy(self.atmosphere.distances, cps, label=channel)

        if plot == 'signals':
            plt.ylabel("Count Rate (Hz)")

        if plot != 'none':
            plt.title('Expected light on sensor')
            plt.legend()
            plt.xlabel('Distance(m)')

        if output_file is not None:
            data = dict(Range=self.atmosphere.distances, Altitude=self.atmosphere.altitudes)
            for channel in channels:
                data[channel] = signals[channels.index(channel)]
            df = pd.DataFrame.from_dict(data)
            df.to_csv(output_file)

            plt.show()

        return channels, signals

    def runall(self, plot='none', output_file=None, signal_noise=False):
        """
        Runs a simulation of the lidar signals.

        Parameters
        ----------
        plot : str
            Type of plot to draw, use 'signals' or 'none' (default).
        output_file : str
            Path for the output csv file, ignore to run without saving.
        signal_noise: bool
            Enables or disables signal noise. Default: False

        Returns
        -------
        channels : list of str
            List of channel names.
        signals : list of ndarray
            List of signals as dicts.
        """

        signals = []
        channels = list(self.settings['Optics']['Channels'].keys())
        if not plot == 'none':
            plt.figure()

        for channel in channels:
            signal = dict()
            signal['PC'], signal['PC_NOISE'], signal['AN'], signal['AN_NOISE'], = electronics.calculate_signal(
                optics.calculate_return_signal(channel, self.atmosphere, self.settings), self.settings, signal_noise)
            signals.append(signal)
            if not plot == 'none':
                plt.plot(self.atmosphere.distances, signal['PC'], label=channel + '_PC')

        if plot == 'signals':
            plt.ylabel("Count Rate (MHz)")

        if plot != 'none':
            plt.title('PC Signal')
            plt.legend()
            plt.xlabel('Distance(m)')

        if output_file is not None:
            data = dict(Range=self.atmosphere.distances, Altitude=self.atmosphere.altitudes)
            for channel in channels:
                for key, sig in signals[channels.index(channel)].items():
                    data[channel + '_' + key] = sig
            df = pd.DataFrame.from_dict(data)
            df.to_csv(output_file)

            plt.show()

        return channels, signals
