from lidarsimulator.simulation import LidarSimulator

if __name__ == '__main__':
    ls = LidarSimulator('Atmosphere.csv', 'Overlap.csv', 'HSRL_Settings.yaml')
    ls.runall_light(plot='signals', output_file='light_output.csv')
    ls.runall(plot='signals', output_file='signal_output.csv')
